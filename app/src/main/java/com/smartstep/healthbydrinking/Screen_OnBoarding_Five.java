package com.smartstep.healthbydrinking;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartstep.healthbydrinking.base.MasterBaseFragment;


public class Screen_OnBoarding_Five extends MasterBaseFragment
{
	View item_view;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		item_view=inflater.inflate(R.layout.screen_onboarding_five, container, false);

		FindViewById();
		Body();

		return item_view;
	}

	public void setUserVisibleHint(boolean isVisibleToUser) {

		super.setUserVisibleHint(isVisibleToUser);
		if(isVisibleToUser)
		{
			//ah.customAlert("call");
			//actionView();
		}
		else{
			//no
		}

	}

	private void FindViewById()
	{

	}

	private void Body()
	{

	}
}