package com.smartstep.healthbydrinking.utils;

import android.content.res.Resources;

public class BaseFunctions {
    public static String getLanguage(){
        return Resources.getSystem().getConfiguration().locale.getDisplayLanguage();
    }
}
