package com.smartstep.healthbydrinking.base;

import android.app.Application;
import android.content.Context;

import com.smartstep.healthbydrinking.utils.LocaleHelper;

public class App extends Application {

    public static int gift_visible_count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "ar"));
    }
}
