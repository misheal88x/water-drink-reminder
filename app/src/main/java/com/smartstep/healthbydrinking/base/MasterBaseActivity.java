package com.smartstep.healthbydrinking.base;

import android.content.Context;
import android.os.Bundle;

import com.basic.appbasiclibs.BaseActivity;

import com.smartstep.healthbydrinking.utils.DB_Helper;
import com.smartstep.healthbydrinking.utils.LocaleHelper;

public class MasterBaseActivity extends BaseActivity
{
    public DB_Helper dbh;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        LocaleHelper.setLocale(this, "ar");
        dbh=new DB_Helper(MasterBaseActivity.this,MasterBaseActivity.this);
    }
}