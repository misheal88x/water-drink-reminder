package com.smartstep.healthbydrinking;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.smartstep.healthbydrinking.adapter.TipsAdapter;
import com.smartstep.healthbydrinking.base.MasterBaseActivity;

import java.util.ArrayList;
import java.util.List;

public class Screen_Tips extends MasterBaseActivity {

    LinearLayout right_icon_block,left_icon_block;
    AppCompatTextView lbl_toolbar_title;
    RecyclerView historyRecyclerView;
    List<String> tips=new ArrayList<>();
    TipsAdapter tipsAdapter;
    AppCompatTextView lbl_no_record_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen__tips);

        FindViewById();
        Body();
    }

    private void FindViewById()
    {
        right_icon_block=findViewById(R.id.right_icon_block);
        left_icon_block=findViewById(R.id.left_icon_block);
        lbl_toolbar_title=findViewById(R.id.lbl_toolbar_title);

        historyRecyclerView=findViewById(R.id.historyRecyclerView);
        lbl_no_record_found=findViewById(R.id.lbl_no_record_found);


    }

    private void Body()
    {
        lbl_toolbar_title.setText(sh.get_string(R.string.str_tips));
        left_icon_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        right_icon_block.setVisibility(View.GONE);

        historyRecyclerView.setNestedScrollingEnabled(false);

        String[] tips_array = new Gson().fromJson(ph.getString("tips_list"),String[].class);
        for (String s : tips_array){
            tips.add(s);
        }
        tipsAdapter = new TipsAdapter(Screen_Tips.this,tips);

        historyRecyclerView.setLayoutManager(new LinearLayoutManager(act, LinearLayoutManager.VERTICAL, false));

        historyRecyclerView.setAdapter(tipsAdapter);
    }
}
