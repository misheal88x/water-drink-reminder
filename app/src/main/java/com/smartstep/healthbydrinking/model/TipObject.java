package com.smartstep.healthbydrinking.model;

import com.google.gson.annotations.SerializedName;

public class TipObject {
    @SerializedName("text") private String text = "";
    @SerializedName("image") private int image ;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
